<?php

namespace Bannister\UserBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * The base user class for applications using the Bannister User Bundle.
 *
 * Class BaseUser
 * @package Bannister\UserBundle
 *
 * @ORM\MappedSuperclass
 * @ORM\EntityListeners({"Bannister\UserBundle\EventListener\DoctrineUserListener"})
 */
class BaseUser implements UserInterface, PasswordAuthenticatedUserInterface
{
    /**
     * The timeout for password requests in seconds, defaults to 5 days.
     */
    const PASSWORD_REQUEST_TIMEOUT = 432000;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected ?int $id;

    /**
     * The date & time at which a user activation e-mail was sent or a user requested a password reset.
     *
     * @ORM\Column(name="date_password_requested", type="datetime", nullable=true)
     *
     * @var null|DateTime $datePasswordRequested
     */
    protected ?DateTime $datePasswordRequested;

    /**
     * @ORM\Column(name="email", type="string", nullable=false)
     *
     * @var string $email
     */
    protected string $email;

    /**
     * Shows if the user was deleted from the application.
     *
     * @ORM\Column(name="is_deleted", type="boolean", nullable=false)
     *
     * @var bool $isDeleted
     */
    protected bool $isDeleted;

    /**
     * Shows if the user account is active and allowed to log in.
     *
     * @ORM\Column(name="is_enabled", type="boolean", nullable=false)
     *
     * @var bool $isEnabled
     */
    protected bool $isEnabled;

    /**
     * The encoded password of a user, which should always be set via the setPlainPassword function.
     *
     * @ORM\Column(name="password", type="string", nullable=false)
     *
     * @var string $password
     */
    protected string $password;

    /**
     * The identifier token used to validate activation or password reset requests.
     *
     * @ORM\Column(name="password_token", type="string", unique=true, nullable=true)
     *
     * @var null|string $passwordToken
     */
    protected ?string $passwordToken;

    /**
     * The plain password is used to set the user password after encoding.
     * This property should never be persisted to the database.
     *
     * @var null|string $plainPassword
     */
    protected ?string $plainPassword;

    /**
     * The Symfony list of user roles that a certain user has.
     *
     * @ORM\Column(name="roles", type="array", nullable=false)
     *
     * @var array $roles
     */
    protected array $roles;

    /**
     * @ORM\Column(name="username", type="string", nullable=true)
     *
     * @var null|string $username
     */
    protected ?string $username;

    /**
     * @ORM\Column(name="date_created", type="datetime", nullable=false)
     *
     * @var DateTime $dateCreated
     */
    protected DateTime $dateCreated;

    /**
     * @ORM\Column(name="date_modified", type="datetime", nullable=false)
     *
     * @var DateTime $dateModified
     */
    protected DateTime $dateModified;

    /**
     * @throws Exception
     */
    public function __construct()
    {
        $datetime = new DateTime();
        $this->dateCreated = $datetime;
        $this->dateModified = $datetime;
        $this->datePasswordRequested = null;
        $this->isDeleted = false;
        $this->isEnabled = false;
        $this->plainPassword = random_bytes(10);
        $this->passwordToken = null;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUserIdentifier(): string
    {
        return $this->getEmail();
    }

    /**
     * @return bool
     */
    public function hasDatePasswordRequestExpired(): bool
    {
        $datePasswordRequested = $this->getDatePasswordRequested();
        if ($datePasswordRequested === null || ($datePasswordRequested->getTimestamp() + self::PASSWORD_REQUEST_TIMEOUT < time())) {
            return true;
        }

        return false;
    }

    /**
     * @return DateTime|null
     */
    public function getDatePasswordRequested(): ?DateTime
    {
        return $this->datePasswordRequested;
    }

    /**
     * @param DateTime|null $datePasswordRequested
     *
     * @return $this
     */
    public function setDatePasswordRequested(DateTime $datePasswordRequested = null): self
    {
        $this->datePasswordRequested = $datePasswordRequested;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return $this
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDeleted(): bool
    {
        return $this->isDeleted;
    }

    /**
     * @param bool $isDeleted
     *
     * @return $this
     */
    public function setIsDeleted(bool $isDeleted): self
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->isEnabled;
    }

    /**
     * @param bool $isEnabled
     *
     * @return $this
     */
    public function setIsEnabled(bool $isEnabled): self
    {
        $this->isEnabled = $isEnabled;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     *
     * @return $this
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPasswordToken(): ?string
    {
        return $this->passwordToken;
    }

    /**
     * @param string|null $passwordToken
     *
     * @return $this
     */
    public function setPasswordToken(string $passwordToken = null): self
    {
        $this->passwordToken = $passwordToken;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    /**
     * @param string|null $plainPassword
     *
     * @return $this
     */
    public function setPlainPassword(string $plainPassword = null): self
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getRoles(): ?array
    {
        return $this->roles;
    }

    /**
     * @param array $roles
     *
     * @return $this
     */
    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @param string $role
     *
     * @return $this
     */
    public function addRole(string $role): self
    {
        $roles = $this->getRoles();
        $roles[] = $role;
        $roles = array_unique($roles);
        $this->setRoles($roles);

        return $this;
    }

    /**
     * @param string $role
     *
     * @return $this
     */
    public function removeRole(string $role): self
    {
        $roles = $this->getRoles();
        $roleIndex = array_search($role, $roles);
        if ($roleIndex !== false) {
            unset($roles[$roleIndex]);
        }
        $this->setRoles($roles);

        return $this;
    }

    /**
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param string|null $username
     *
     * @return $this
     */
    public function setUsername(string $username = null): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDateCreated(): DateTime
    {
        return $this->dateCreated;
    }

    /**
     * @param DateTime $dateCreated
     *
     * @return $this
     */
    public function setDateCreated(DateTime $dateCreated): self
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDateModified(): DateTime
    {
        return $this->dateModified;
    }

    /**
     * @param DateTime $dateModified
     *
     * @return $this
     */
    public function setDateModified(DateTime $dateModified): self
    {
        $this->dateModified = $dateModified;

        return $this;
    }

    /**
     * Removes sensitive data from the user.
     */
    public function eraseCredentials()
    {
        $this->setPlainPassword(null);
    }

    /**
     * Returns the salt that was originally used to encode the password.
     */
    public function getSalt(): void
    {
        // Not used by the Bannister User Bundle, but required by the UserInterface.
    }
}
