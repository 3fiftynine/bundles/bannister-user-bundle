Form Customization
=============
Introduction
------------
This section will discuss the basics of how to customize the *Bannister User Bundle*'s forms to your needs.

Additions
---------
The first way to alter one of the forms is by adding new fields to an existing one.

Say, for example, you would like the user to provide their phone number in the registration form.
Start off by creating a new form class and extending it from the *Bannister User Bundle*'s form.

```php
// src/Form/PhoneRegistrationType.php

namespace App\Form;

use Bannister\UserBundle\Form\RegistrationType;
use Symfony\Component\Form\FormBuilderInterface;

class PhoneRegistrationType extends RegistrationType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // TODO: Our form field will be added here soon.
    }
}
```

Next up, we will first build the parent form and then add our new field to it.

```php
// src/Form/PhoneRegistrationType.php

namespace App\Form;

use Bannister\UserBundle\Form\RegistrationType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;

class PhoneRegistrationType extends RegistrationType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder->add('phone', NumberType::class, array('label' => 'Phone number'));
    }
}
```

Now that we have a new form containing the original registration fields and our phone number field, simply configure the *Bannister User Bundle* to use the `PhoneRegistrationType` as the registration form.

```yaml
# config/packages/bannister_user.yaml

bannister_user:
    registration:
        form: Bannister\UserBundle\Form\PhoneRegistrationType
```

Replacements
------------
Replacing a form roughly follows the same process as adding on to one.

Having created your own form class, head over to the `bannister_user.yaml` configuration file.
Now replace the default configured form for the flow you wish to alter by your own and the *Bannister User Bundle* will now use your form.

```yaml
# config/packages/bannister_user.yaml

bannister_user:
    registration:
        form: Bannister\UserBundle\Form\RegistrationType
    activation:
        form: Bannister\UserBundle\Form\ActivationType
    login:
        form: Bannister\UserBundle\Form\LoginType
    resetting:
        form_request: Bannister\UserBundle\Form\PasswordRequestType
        form_reset: Bannister\UserBundle\Form\PasswordResetType
```

Table of Contents
-----------------
0. [Bundle installation](../../README.md)
0. [Bannister User Registration](bannister-user-registration.md)
0. [Bannister User Activation](bannister-user-activation.md)
0. [Bannister Login](bannister-login.md)
0. [Bannister Password Resetting](bannister-password-reset.md)
0. [Customizing Bannister Base User](customizing-bannister-base-user.md)
0. [Customizing Bannister Routes](customizing-bannister-routes.md)
0. **[Customizing Bannister Forms](customizing-bannister-forms.md)**
0. [Customizing Bannister Templates](customizing-bannister-templates.md)
0. [Customizing Bannister Controllers](customizing-bannister-controllers.md)
0. [Configuration reference](configuration-reference.md)