User Registration
=============
Introduction
------------
This chapter will discuss the standard functionality offered by the *Bannister User Bundle* for user activation.

Summary
-------
After having registered a new user, an activation e-mail is sent to the new user, which contains a link to the activation page.
The activation page then requests the user to choose their password, and after doing so successfully, they are redirected to the login page.

The *Bannister User Bundle* does not notify the user of invalid or non-existent credentials during the user activation and password reset flows out of security concerns.
Should you require such functionality for your application, you will need to extend the `BannisterActivationController` and add the logic there.

Route
-----
Anonymous users are allowed the approach the `bannister_user_activate_user` route via `/activate-user/activate/{passwordToken}`, which is provided in the user activation e-mail.
The user receives the e-mail and activates their account via the `bannisterSendUserActivationEmail` and `bannisterActivateUser` functions in the `BannisterActivationController`. 

Form
----
The user activation process uses the `ActivationType` form, another simple form with just a repeated `plainPassword` field and a `submit` button.
By default, the form uses the `bannisterUserBundle` translation domain for its form field labels.

Template
--------
The `activation/email.html.twig` Twig template is a bare bones e-mail template extending the `baseEmail.html.twig` template, requiring only a `activationUrl` Twig variable to link the user to the user activation page.

The `activation/activation.html.twig` Twig template is a minimalistic page extending the `base.html.twig` template, and requiring a `form` Twig variable to properly render.
The form is rendered inside a `<div>` element with the `bannister-user-bundle--activation-form` class.

The entirety of the templates are rendered within a `{% block bannisterUserBundle %}` block, making this block mandatory in the extended base templates.

Controller
----------
The `BannisterActivationController`'s `bannisterSendUserActivationEmail` function cannot be called via a route, but is rather forwarded to in your application's back-end when an activation e-mail needs sending.

The function checks whether the user has a pending activation request, and if so, returns a valid response without sending an e-mail.
In the absence of an active activation request, the function starts one by setting the `datePasswordRequested` and `passwordToken` of the user and finishes by sending an e-mail.

The `BannisterActivationController`'s `bannisterActivateUser` function is called via the `bannister_user_activate_user` route, which accepts both `GET` and `POST` in order to render the user activation page and activate the user, respectively. 

On a `GET` request, it generates the user activation form using a user object, retrieved via its `passwordToken` property, and then returns the rendered Twig template.

However, on a `POST` request, the same form is generated and then handles the request data using Symfony's native form handling.
The retrieved user is then activated by emptying the `datePasswordRequested` and `passwordToken` properties, setting the password via `plainPassword`, and setting the `isEnabled` property to `true`. 
Subsequently, the user is redirected to the login page.

It is possible to customize the redirect on activation via the *Bannister User Bundle* configuration, with the option of enabling authentication to allow a redirect to somewhere inside your application.

Table of Contents
-----------------
0. [Bundle installation](../../README.md)
0. [Bannister User Registration](bannister-user-registration.md)
0. **[Bannister User Activation](bannister-user-activation.md)**
0. [Bannister Login](bannister-login.md)
0. [Bannister Password Resetting](bannister-password-reset.md)
0. [Customizing Bannister Base User](customizing-bannister-base-user.md)
0. [Customizing Bannister Routes](customizing-bannister-routes.md)
0. [Customizing Bannister Forms](customizing-bannister-forms.md)
0. [Customizing Bannister Templates](customizing-bannister-templates.md)
0. [Customizing Bannister Controllers](customizing-bannister-controllers.md)
0. [Configuration reference](configuration-reference.md)