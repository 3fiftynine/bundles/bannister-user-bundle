Controller Customization
=============
Introduction
------------
This section will discuss the basics of how to customize the *Bannister User Bundle*'s controllers to your needs.

Customization
-------------
The customization of the *Bannister User Bundle* controllers is the most difficult to do, but luckily still isn't too hard.
To properly explain how to achieve this, we will make use of an example in which we limit user registration to e-mail addresses with the `@gmail.com` domain.

Since we will be altering the registration process, create yourself a new controller class (e.g. `RegistrationController`), and extend it from the `BannisterSecurityController`.

```php
// src/Controller/RegistrationController.php

namespace App\Controller;

use Bannister\UserBundle\Controller\BannisterSecurityController;

class RegistrationController extends BannisterSecurityController
{
    // TODO: We will add our customized registration code here.
}
```

Next, create a new function to handle the registration.
Within this function, we will retrieve the registration form from the incoming request and check whether it is a Gmail address.
We will keep this simple, so if there is no match, we will simply throw an exception.
If there is a match, however, we will simply let it carry on with the registration process.

```php
// src/Controller/RegistrationController.php

namespace App\Controller;

use Bannister\UserBundle\Controller\BannisterSecurityController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class RegistrationController extends BannisterSecurityController
{
    public function registerUserAction(EntityManagerInterface $entityManager, Request $request)
    {
        $email = $request->get('email');
        preg_match('/^.+@gmail.com$/', $email, $allowed);
        
        if(empty($allowed)) {
            throw new AccessDeniedHttpException();
        }
        
        parent::bannisterRegistration($entityManager, $request);
    }
}
```

That is all that is needed for customizing the functionality.
However, we are not finished yet, as we need to add a route to ensure the registration process does not skip over our new controller.
Grant our new function its very own route and add it to the `bannister_user.yaml` configuration file.

```php
// src/Controller/RegistrationController.php

namespace App\Controller;

use Bannister\UserBundle\Controller\BannisterSecurityController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;

class RegistrationController extends BannisterSecurityController
{
    /**
     * @Route("/register-user", name="registerUser")
     */
    public function registerUserAction(EntityManagerInterface $entityManager, Request $request)
    {
        $email = $request->get('email');
        preg_match('/^.+@gmail.com$/', $email, $allowed);

        if(empty($allowed)) {
            throw new AccessDeniedHttpException();
        }

        parent::bannisterRegistration($entityManager, $request);
    }
}
```

```yaml
# config/packages/bannister_user.yaml

bannister_user:
    registration:
        route: registerUser
```

That is all, whenever you register a user via the `registerUser` route now, it should validate the e-mail address in the process!

Table of Contents
-----------------
0. [Bundle installation](../../README.md)
0. [Bannister User Registration](bannister-user-registration.md)
0. [Bannister User Activation](bannister-user-activation.md)
0. [Bannister Login](bannister-login.md)
0. [Bannister Password Resetting](bannister-password-reset.md)
0. [Customizing Bannister Base User](customizing-bannister-base-user.md)
0. [Customizing Bannister Routes](customizing-bannister-routes.md)
0. [Customizing Bannister Forms](customizing-bannister-forms.md)
0. [Customizing Bannister Templates](customizing-bannister-templates.md)
0. **[Customizing Bannister Controllers](customizing-bannister-controllers.md)**
0. [Configuration reference](configuration-reference.md)