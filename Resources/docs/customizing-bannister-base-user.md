Base User Customization
=============
Introduction
------------
This section will show you the basics of how to customize the *Bannister User Bundle*'s `BaseUser` class to your needs.
We will show this by adding a new property to the basic user class and overriding an existing one.

Before
------
The most basic user class utilizing the *Bannister User Bundle* should look something like this:

```php
// src/Entity/User.php

namespace App\Entity;

use Bannister\UserBundle\Entity\BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User extends BaseUser
{
    // TODO: Our work will be done here.
}
```

Additional Properties
---------------------
First, we are going to add a property to the user class which will allow us to differentiate between trial users and regular users.
The process of adding a new property to the base user class is no different than normally adding properties to an entity.

Start off by adding a `private` variable `$isTrial` to the entity and defining the column properties `name`, `type`, and `nullable`.
Since our users can only be part of a trial account or not, the non-nullable boolean type is a perfect solution.

```php
// src/Entity/User.php

/**
 * @ORM\Column(name="is_trial", type="boolean", nullable=false) 
 */
private $isTrial;
```

That's actually all there is to it!
Now you can just update your database with the new field and Doctrine will take care of the rest.

Oh right, and don't forget to add a getter and setter.

```php
// src/Entity/User.php

public function isTrial(): bool
{
    return $this->isTrial;
}

public function setIsTrial(bool $isTrial): self
{
    $this->isTrial = $isTrial;

    return $this;
}
```

Overriding Existing Properties
------------------------------
For our second trick, we are going to make our users' e-mail addresses unique in order to use them as part of their login credentials.

This part might actually be simpler than adding a new property, as we can just copy the e-mail property from the `BaseUser` class and add onto it.

```php
// src/Entity/User.php

/**
 * @ORM\Column(name="email", type="string", nullable=false)
 *
 * @var string $email
 */
protected $email;
```

Adding a unique index to this property is as simple as setting the `unique` property to `true` in the column properties.
If you want, you can even get rid of the type hint in the PHPDoc.

```php
// src/Entity/User.php

/**
 * @ORM\Column(name="email", type="string", nullable=false, unique=true)
 */
protected $email;
```

That's it!
If you merely wanted to update the column properties, that is all there is to it. 
You don't even have to define getters and setters, as those are in the `BaseUser` class.

If you did want to add new logic to the getters and setters, you can override them in the same way you did the `email` property. 

After
-----
After your work is done, the user class should look something like the one shown below.

```php
// src/Entity/User.php

namespace App\Entity;

use Bannister\UserBundle\Entity\BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Column(name="email", type="string", nullable=false, unique=true)
     */
    protected $email;

    /**
     * @ORM\Column(name="is_trial", type="boolean", nullable=false)
     */
    private $isTrial;

    public function isTrial(): bool
    {
        return $this->isTrial;
    }

    public function setIsTrial(bool $isTrial): self
    {
        $this->isTrial = $isTrial;

        return $this;
    }
}
```

Table of Contents
-----------------
0. [Bundle installation](../../README.md)
0. [Bannister User Registration](bannister-user-registration.md)
0. [Bannister User Activation](bannister-user-activation.md)
0. [Bannister Login](bannister-login.md)
0. [Bannister Password Resetting](bannister-password-reset.md)
0. **[Customizing Bannister Base User](customizing-bannister-base-user.md)**
0. [Customizing Bannister Routes](customizing-bannister-routes.md)
0. [Customizing Bannister Forms](customizing-bannister-forms.md)
0. [Customizing Bannister Templates](customizing-bannister-templates.md)
0. [Customizing Bannister Controllers](customizing-bannister-controllers.md)
0. [Configuration reference](configuration-reference.md)