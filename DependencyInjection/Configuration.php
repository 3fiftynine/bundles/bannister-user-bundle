<?php

declare(strict_types=1);

namespace Bannister\UserBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 * @package Bannister\UserBundle\DependencyInjection
 */
class Configuration implements ConfigurationInterface
{
    /**
     * Generates the configuration tree builder.
     *
     * @return TreeBuilder The tree builder
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('bannister_user');
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->addDefaultsIfNotSet()
            ->children()
            ->scalarNode('firewall')
            ->info('The name of your configured firewall.')
            ->defaultValue('main')
            ->cannotBeEmpty()
            ->end()
            ->scalarNode('translation_domain')
            ->info('The translation domain used for e-mail subjects.')
            ->defaultValue('bannisterUserBundle')
            ->cannotBeEmpty()
            ->end()
            ->scalarNode('user_entity_class')
            ->info('Your User entity class which extends the Bannister BaseUser class.')
            ->defaultValue('App/Entity/User')
            ->cannotBeEmpty()
            ->end()
            ->end();

        $this->addEmailConfiguration($rootNode);
        $this->addRegistrationConfiguration($rootNode);
        $this->addActivationConfiguration($rootNode);
        $this->addLoginConfiguration($rootNode);
        $this->addResettingConfiguration($rootNode);

        return $treeBuilder;
    }

    /**
     * @param ArrayNodeDefinition $rootNode
     */
    private function addEmailConfiguration(ArrayNodeDefinition $rootNode): void
    {
        $rootNode
            ->children()
            ->arrayNode('email')
            ->addDefaultsIfNotSet()
            ->children()
            ->scalarNode('from_address')
            ->defaultValue('Support@Bannister.com')
            ->cannotBeEmpty()
            ->end()
            ->scalarNode('from_name')
            ->defaultValue('Bannister User Support')
            ->cannotBeEmpty()
            ->end()
            ->end()
            ->end();
    }

    /**
     * @param ArrayNodeDefinition $rootNode
     */
    private function addRegistrationConfiguration(ArrayNodeDefinition $rootNode): void
    {
        $rootNode
            ->children()
            ->arrayNode('registration')
            ->children()
            ->scalarNode('form')
            ->defaultValue('Bannister\UserBundle\Form\RegistrationType')
            ->cannotBeEmpty()
            ->end()
            ->scalarNode('page_template')
            ->defaultValue('@BannisterUser/security/registration.html.twig')
            ->cannotBeEmpty()
            ->end()
            ->scalarNode('route')
            ->defaultValue('bannister_user_registration')
            ->cannotBeEmpty()
            ->end()
            ->scalarNode('on_success_route')
            ->defaultValue('bannister_user_login')
            ->cannotBeEmpty()
            ->end()
            ->end()
            ->end();
    }

    /**
     * @param ArrayNodeDefinition $rootNode
     */
    private function addActivationConfiguration(ArrayNodeDefinition $rootNode): void
    {
        $rootNode
            ->children()
            ->arrayNode('activation')
            ->children()
            ->scalarNode('form')
            ->defaultValue('Bannister\UserBundle\Form\ActivationType')
            ->cannotBeEmpty()
            ->end()
            ->scalarNode('email_subject')
            ->defaultValue('activation.subject')
            ->cannotBeEmpty()
            ->end()
            ->scalarNode('email_template')
            ->defaultValue('@BannisterUser/activation/email.html.twig')
            ->cannotBeEmpty()
            ->end()
            ->scalarNode('page_template')
            ->defaultValue('@BannisterUser/activation/activation.html.twig')
            ->cannotBeEmpty()
            ->end()
            ->booleanNode('authentication_on_success')
            ->defaultValue(false)
            ->end()
            ->scalarNode('on_success_route')
            ->defaultValue('bannister_user_login')
            ->cannotBeEmpty()
            ->end()
            ->scalarNode('route')
            ->defaultValue('bannister_user_activate_user')
            ->cannotBeEmpty()
            ->end()
            ->end()
            ->end();
    }

    /**
     * @param ArrayNodeDefinition $rootNode
     */
    private function addLoginConfiguration(ArrayNodeDefinition $rootNode): void
    {
        $rootNode
            ->children()
            ->arrayNode('login')
            ->children()
            ->scalarNode('form')
            ->defaultValue('Bannister\UserBundle\Form\LoginType')
            ->cannotBeEmpty()
            ->end()
            ->scalarNode('page_template')
            ->defaultValue('@BannisterUser/security/login.html.twig')
            ->cannotBeEmpty()
            ->end()
            ->scalarNode('route')
            ->defaultValue('bannister_user_login')
            ->cannotBeEmpty()
            ->end()
            ->end()
            ->end();
    }

    /**
     * @param ArrayNodeDefinition $rootNode
     */
    private function addResettingConfiguration(ArrayNodeDefinition $rootNode): void
    {
        $rootNode
            ->children()
            ->arrayNode('resetting')
            ->children()
            ->scalarNode('form_request')
            ->defaultValue('Bannister\UserBundle\Form\PasswordRequestType')
            ->cannotBeEmpty()
            ->end()
            ->scalarNode('form_reset')
            ->defaultValue('Bannister\UserBundle\Form\PasswordResetType')
            ->cannotBeEmpty()
            ->end()
            ->scalarNode('email_subject')
            ->defaultValue('passwordReset.subject')
            ->cannotBeEmpty()
            ->end()
            ->scalarNode('email_template')
            ->defaultValue('@BannisterUser/password-reset/email.html.twig')
            ->end()
            ->scalarNode('page_template_request')
            ->defaultValue('@BannisterUser/password-reset/request.html.twig')
            ->end()
            ->scalarNode('page_template_reset')
            ->defaultValue('@BannisterUser/password-reset/reset.html.twig')
            ->end()
            ->scalarNode('route_request')
            ->defaultValue('bannister_user_password_request')
            ->cannotBeEmpty()
            ->end()
            ->scalarNode('route_reset')
            ->defaultValue('bannister_user_password_reset')
            ->cannotBeEmpty()
            ->end()
            ->booleanNode('authentication_on_success_request')
            ->defaultValue(true)
            ->end()
            ->booleanNode('authentication_on_success_reset')
            ->defaultValue(true)
            ->end()
            ->scalarNode('on_success_route_request')
            ->defaultValue('bannister_user_login')
            ->cannotBeEmpty()
            ->end()
            ->scalarNode('on_success_route_reset')
            ->defaultValue('bannister_user_login')
            ->cannotBeEmpty()
            ->end()
            ->end()
            ->end();
    }
}
